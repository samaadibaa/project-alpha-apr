from django.shortcuts import render
from projects.models import Project
from django.contrib.auth.decorators import login_required
from tasks.models import Task


@login_required
def projects_list(request):
    project_lists = Project.objects.filter(owner=request.user)
    print(project_lists)
    context = {
        "projects_list": project_lists,
    }
    return render(request, "list.html", context)


@login_required
def show_project(request, id):
    project_tasks = Task.objects.filter(id=id)
    context = {
        "project_tasks": project_tasks,
    }
    return render(request, "detail.html", context)
